#include <iostream>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <mpi.h>

using namespace std;

struct label {
   int nro;	/* numero del nodo */
   int prev;	/* nodo precedente (-1 para el nodo inicial )*/
   int peso;	/* peso o coste total de la trayectoria que
				 * conduce al nodo, i.e., el coste total desde
				 * el nodo inicial hasta el actual. Un valor
				 * de -1 denota el infinito */
   int marca;	/* si el nodo ha sido marcado o no */
};
typedef struct label label_t;

string L1[]={"San Pablo","Neptuno","Pajaritos","Las Rejas","Ecuador","San Alberto Hurtado","Universidad de Santiago"
            ,"Estacion Central","Union Latinoamericana","Republica","Los Heroes","La Moneda","Universidad de Chile"
            ,"Santa Lucia","Universidad Catolica","Baquedano","Salvador","Manuel Montt","Pedro de Valdivia"
            ,"Los Leones","Tobalaba","El Golf","Alcantara","Escuela Militar","Manquehue","Hernando de Magallanes"
            ,"Los Dominicos","Vespucio Norte","Zapadores","Dorsal","Einstein","Cementerios"
            ,"Cerro Blanco","Patronato","Puente Cal y Canto","Santa Ana","Toesca","Parque OHiggins","Rondizzoni"
            ,"Franklin","El Llano","San Miguel","Lo Vial","Departamental","Ciudad del Nino","Lo Ovalle","El Parron"
            ,"La Cisterna","Cristobal Colon","Francisco Bilbao","Principe de Gales","Simon Bolivar","Plaza Egana"
            ,"Los Orientales","Grecia","Los Precidentes","Quilin","Las Torres","Macul","Vicu�a Mackenna"
            ,"Vicente Valdes","Rojas Magallanes","Trinidad","San Jose de la Estrella","Los Quillayes","Elisa Correa"
            ,"Hospital Sotero del Rio","Protectora de la Infancia","Las Mercedes","Plaza de Puente Alto"
            ,"Santa Julia","La Granja","Santa Rosa","San Ramon","Plaza de Maipu","Santiago Bueras","Del Sol"
            ,"Monte Tabor","Las Parcelas","Laguna Sur","Barrancas","Pudahuel","Lo Prado","Blanqueado"
            ,"Gruta de Lourdes","Quinta Normal","Cumming","Plaza de Armas","Bellas Artes","Parque Bustamante"
            ,"Santa Isabel","Irarrazaval","Nuble","Rodrigo de Araya","Carlos Valdovinos","Camino Agricola"
            ,"San Joaquin","Pedrero","Mirador","Bellavista de La Florida","Cerrillos","Lo Valledor"
            ,"Presidente Pedro Aguirre Cerda","Bio Bio","Estadio Nacional","Nunoa","Ines de Suares"};

string Estaciones_Codigo[]={"SP","NP","PJ","LR","EC","AH","US","EL","LA","RP","LH","LM","CH","SL","UC","BA","SA","MM"
                        ,"PV","LE","TB","GO","AL","EM","MQ","HM","LD","AV","ZA","DO","EI","CE","CB","PT","CA","AN"
                        ,"TO","PQ","RO","FR","LL","SM","LV","DE","CN","LO","EP","LC","COL","BIL","PDG","SBO","PEG"
                        ,"LOR","RGR","LPR","RQU","LTO","MAC","VMA","VVA","RMA","TRI","SJE","LQU","ECO","HSR","PIN"
                        ,"LME","PPA","SJU","LGR","SRO","SRA","PM","SB","DS","MT","LP","LS","BR","PU","PR","BL","GL"
                        ,"QN","RC","PZ","BE","PB","SI","IR","NU","RA","CV","AG","SJ","PE","MA","LF","CER","LVA","PAC"
                        ,"BIO","ENS","NUO","ISU"};

void Crear_Matriz(int** Matriz);
void Mostrar_Matriz(int** Matriz);
void dijkstra( int N, int **A, int a, int b,string L[106],int argc,char *argv[] );

#include <iostream>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <mpi.h>
#include <cstring>

using namespace std;

int main(int argc,char *argv[]){

    int** Metro;
    int filas = 107, columnas = 107;   /*Supongamos esos valores*/
    Metro = (int **)malloc(filas * sizeof(int *));

    for(int i = 0; i < columnas; i++){
        Metro[i] = (int *)malloc(columnas * sizeof(int));
    }
    int intento=0,j,k,s;
    char *ptr;
    char opcion[100];
    do{
        cout << "CONTROL COMPUTACION PARALELA Y DISTRIBUIDA" << endl;
        cout <<"============================================"<<endl;
        cout <<" Opcion 1 "<< endl;
        cout << " -f 'codigo estacion origen' 'codigo estacion destino', para calcular ruta minima entre dos estaciones"<<endl;
        cout << endl;
        cout << " Opcion 2" << endl;
        cout << " -v, que muestra los integrantes del grupo con sus datos" << endl;
        cout <<"============================================"<<endl;
        cout << endl;
        cout << " Opcion 3" << endl;
        cout << " -l, ver estaciones" << endl;
        cout <<"============================================"<<endl;
        cout << endl;
        cout << " Opcion 4" << endl;
        cout << " -s, Salir" << endl;
        cout <<"============================================"<<endl;
        cout << endl;
        cout << "Ingrese opcion bajo el formato especificado: ";
        cin.getline(opcion,100);cin.ignore();
        //char* y=opcion;
        ptr = strtok(opcion," ");//argumentos: frase, caracter delimitador
        while(ptr != NULL)
        {
            //cout << ptr<<endl;
            string local=ptr;
            //cout << local << endl;
            //if(intento==)
            if(intento == 2){
                for(int i=0;i<107;i++){
                        cout << local<<"-"<<Estaciones_Codigo[i]<<"-"<<intento << endl;
                    if(local==Estaciones_Codigo[i] )
                    {
                        k=i;
                        intento++;
                    }
                }
                if (intento == 2) intento = 5;
            }else if(intento =1){
                for(int i=0;i<107;i++){

                    if(local==Estaciones_Codigo[i] ){
                        intento++;
                        j=i;
                    }
                }
                if (intento ==1) intento = 5;
            }else if(intento = 0){
                if(local=="-s"){
                    intento=-3;
                break;}
                else if(local=="-v"){
                    intento=-1;
                    break;}
                else if(local== "-l"){
                    intento=-2;
                    break;}
                else if(local== "-f") {intento++;
                }else {break;}
            }else break;
            ptr = strtok(NULL, " ");
            //intento++;
            //cout << ptr << endl;

        }

        if (intento == 3){
            system("cls");
            Crear_Matriz(Metro);
            //Mostrar_Matriz(Metro);
            dijkstra(107,Metro,j,k,L1,argc,argv);//si cumple todos los requisitos entonces pasa a la ejecucucion
            system("pause");
            system("cls");



        }else if(intento == -1){

            cout <<endl;
            cout << "Integrantes: " <<endl;
            cout << "- Felipe Reyes Gutierrez" << endl;
            cout << "- Fabian Araya Leon"<<endl;
            cout << "- Nelson Roa Inostroza" << endl;
            return 0;

        }else if(intento == 2){
            cout << "La estacion de destino es incorrecta" << endl;
            intento=0;
            system("pause");
            system("cls");

        }else if(intento == 1){
            cout << "La estacion de partida es incorrecta" << endl;
            intento=0;
            system("pause");
            system("cls");

        }else if(intento == 0){
            cout << "La primera exprecion es incorrecta" << endl;
            intento=0;
            system("pause");
            system("cls");
        }
        else if(intento == -2){
            intento=0;

        }else{
            cout << "Escriba un comando correcto" << endl;
            intento=0;

            //system("pause");
            system("cls");
        }
        //system("pause");
        s=-3;
        //system("cls");
    }while(s!=-3);
        //cout << "LA FRASE DESPUES: " << frase << endl;

    return 0;

}

void dijkstra( int N, int **A, int a, int b ,string L[106],int argc,char *argv[]) {

   label_t *Labels;
   int i, i0, j, peso;
   int *ruta;		/* array de nodos de la ruta minima */
   /* Crea din'amicamente el arreglo de etiquetas de nodo */
   if ( ( Labels = new label_t[N] ) == NULL ){return;}
   /* nodo inicial 'a' entre 0 y N - 1 */
   if ( a < 0 || a > N - 1 ) return;
   /* nodo final 'a' entre 0 y N - 1 */
   if ( b < 0 || b > N - 1 ) return;
   /* inicializar las etiquetas de nodo */
    int n=107,myid, numprocs;
    double mypi;
    //MPI_Status status;

    MPI_Init(&argc,&argv);
    MPI_Comm_size(MPI_COMM_WORLD, &numprocs);
    MPI_Comm_rank(MPI_COMM_WORLD, &myid);
    // El proceso 0 env�a el n�mero de intervalos
    // al resto de los procesos
    MPI_Bcast(&n, 1, MPI_INT, 0, MPI_COMM_WORLD);

   for ( i = myid; i < n; i+=numprocs ) {
      Labels[i].nro = i;
      if ( i != a ) {
         Labels[i].prev = -1;	/* a'un no se ha definido predecesor */
         Labels[i].peso = -1;	/* infinito */
         Labels[i].marca = 0;
      }
      else {
         Labels[i].prev = -1;	/* a'un no se ha definido predecesor */
         Labels[i].peso = 0;		/* coste del nodo inicial a s'i mismo es cero */
         Labels[i].marca = 0;
      }
   }
   printf("\nProceso:%d  \n", myid);
   MPI_Finalize();
   /* continuamos este ciclo mientras existan nodos no marcados */
   while ( 1 ) {
      /* busca entre todos los nodos no marcados el de menor peso, descartando los
       * de peso infinito (-1) */
      peso = -1;
      i0 = -1;
      for ( i = 0; i < N; i++ ) {
         if ( Labels[i].marca == 0 && Labels[i].peso >= 0 )
            if ( peso == -1 ) {
               peso = Labels[i].peso;
               i0 = i;
            }
            else if ( Labels[i].peso <= peso ) {
               peso = Labels[i].peso;
               i0 = i;
            }
      }
      if ( i0 == -1 ) {	/* termina si no encuentra */
         //cout << "Ya no quedan nodos por analizar." << endl;
         break;
      }
      //cout << "*** Analizando nodo " << i0 << " ***" << endl;

      /* actualiza el peso de todos los sucesores (si los hay) del nodo
       * encontrado y luego se~nala dicho nodo como marcado */
       int cont=0;
      for ( i = 0; i < N; i++ ) {
         if ( A[i0][i] > 0 ) {
            /* si el coste acumulado sumado al coste del enlace del nodo i0 al nodo i
             * es menor al coste del nodo i (o si el coste del nodo i es infinito),
             * debemos actualizar */
            if ( Labels[i].peso == -1 || Labels[i0].peso + A[i0][i] < Labels[i].peso ) {
               if ( Labels[i0].peso + A[i0][i] < Labels[i].peso )
                  cout << "   [ mejorando coste de estacion " << L[i] << " ]" << endl;
               Labels[i].peso = Labels[i0].peso + A[i0][i];
               Labels[i].prev = i0;
               cout << "   coste desde " << L[a] << " hasta " << L[i] << ": " << Labels[i].peso << endl;
            }
         }

      }
      Labels[i0].marca = 1;
      //cout << "   [ nodo " << i0 << " marcado ]" << endl;

      /* para verificar, imprime los costes calculados hasta el momento */
      /*for ( i = 0; i < N; i++ ) {
         cout << i << ": [";
         if ( Labels[i].peso == -1 ) cout << "Inf";
         else cout << Labels[i].peso;
         cout << ", " << Labels[i].prev ;
         if ( Labels[i].marca == 1 ) cout <<  ", x]" << endl;
         else cout << "]" << endl;
      }
      cout << endl;
*/
      /* pausa (opcional) */
      //cout << "presione ENTER para continuar ...";
      //cin.get();
   }

   /* Ruta desde el nodo 'a' hasta el nodo 'b' */
   int longitud = 2;
   i = b;
   while ( ( i = Labels[i].prev ) != a ) longitud++;	/* primero estimamos la longitud de la ruta */
   if ( ( ruta = new int[longitud] ) == NULL ) return;

   ruta[longitud - 1] = b;		/* luego rellenamos */
   i = b;
   j = 0;
   for ( j = 1; j < longitud; j++ ) {
      i = Labels[i].prev;
      ruta[longitud - j - 1] = i;
   }

   cout << "================================================================" << endl;
   cout << endl << "Ruta mas economica entre: " << L[a] << " y " << L[b] << ":" << endl << endl;
   for ( i = 0; i < longitud; i++ ) {
      cout << L[ruta[i]];
      if ( i < longitud - 1 ) cout << " - ";
   }
   cout << endl << endl << "Numero de estaciones: " << Labels[b].peso << endl << endl;

   delete ruta;
   delete [] Labels;
}

void Crear_Matriz(int** Matriz){
    int valor;
    for(int i=0;i<107;i++){
        for(int j=0;j<107;j++){
            if(i == 0 or i==27 or i==48 or i==70 or i==74 or i==100){//Evita Conecciones entre diferentes Lineas
                if(j-i==1){
                    Matriz[i][j]=1;
                }else{
                    Matriz[i][j]=0;
                }
            }else if(i == 26 or i==47 or i==69 or i==73 or i==99 or i==106){//Evita Conecciones entre diferentes Lineas
                if(i-j==1){
                    Matriz[i][j]=1;
                }else{
                    Matriz[i][j]=0;
                }
            }else if(0<i<26){//Conectando estaciones de Linea 1
                if((j-i==1) or (i-j==1)){
                    //cout << "hola";
                    Matriz[i][j]=1;
                }else{
                    Matriz[i][j]=0;
                }

            }else if(27<i<47){//Conectando estaciones de Linea 2
                if((j-i==1) or (i-j==1)){
                    Matriz[i][j]=1;
                }else{
                    Matriz[i][j]=0;
                }
            }else if((48<i<69) ){//Conectando estaciones de Linea 4
                if((j-i==1) or (i-j==1)){
                    Matriz[i][j]=1;
                }else{
                    Matriz[i][j]=0;
                }
            }else if((70<i<73) ){//Conectando estaciones de Linea 4A
                if((j-i==1) or (i-j==1)){
                    Matriz[i][j]=1;
                }else{
                    Matriz[i][j]=0;
                }
            }else if((74<i<99) ){//Conectando estaciones de linea 5
                if((j-i==1) or (i-j==1)){
                    Matriz[i][j]=1;
                }else{
                    Matriz[i][j]=0;
                }
            }else if((100<i<106) ){//Conectando estaciones de Linea 6
                if((j-i==1) or (i-j==1)){
                    Matriz[i][j]=1;
                }else{
                    Matriz[i][j]=0;
                }
            }else{
                Matriz[i][j]=0;
            }
        }
    }


    for(int i=0;i<107;i++){
        for(int j=0;j<107;j++){
            if((i==0 and j==81) or (j==0 and i==81)){//Uniendo San Pablo con Pudahuel
                Matriz[i][j]=1;
            }else if((i==0 and j==82) or (j==0 and i==82)){//Uniendo San Pablo con Lo Prado
                Matriz[i][j]=1;
            }else if((i==10 and j==35) or (j==10 and i==35)){//Uniendo Los Heroes con Santa Ana
                Matriz[i][j]=1;
            }else if((i==10 and j==36) or (j==10 and i==36)){//Uniendo Los Heroe con Toesca
                Matriz[i][j]=1;
            }else if((i==15 and j==88) or (j==15 and i==88)){//Uniendo Baquedano con Bellas Artes
                Matriz[i][j]=1;
            }else if((i==15 and j==89) or (j==15 and i==89)){//Uniendo Baquedano con Parque Bustamante
                Matriz[i][j]=1;
            }else if((i==19 and j==106) or (j==19 and i==106)){//Uniendo Los Leones con Ines de Suarez
                Matriz[i][j]=1;
            }else if((i==20 and j==48) or (j==20 and i==48)){//Uniendo Tobalaba con Cristobal Colon
                Matriz[i][j]=1;
            }else if((i==35 and j==86) or (j==35 and i==86)){//Uniendo Santa Ana con Cumming
                Matriz[i][j]=1;
            }else if((i==35 and j==87) or (j==35 and i==87)){//Uniendo Santa Ana con Plaza de Armas
                Matriz[i][j]=1;
            }else if((i==39 and j==102) or (j==39 and i==102)){//Uniendo Franklin con Presidente Pedro Aguirre Cerda
                Matriz[i][j]=1;
            }else if((i==39 and j==103) or (j==39 and i==103)){//Uniendo Franklin con Bio Bio
                Matriz[i][j]=1;
            }else if((i==47 and j==73) or (j==47 and i==73)){//Uniendo La Cisterna con San Ramon
                Matriz[i][j]=1;
            }else if((i==59 and j==70) or (j==59 and i==70)){//Uniendo Vicuna Mackenna con Santa Julia
                Matriz[i][j]=1;
            }else if((i==60 and j==99) or (j==60 and i==99)){//Uniendo Vicente Valdes con Bellavista de La Florida
                Matriz[i][j]=1;
            }else if((i==92 and j==103) or (j==92 and i==103)){//Uniendo Nuble con Bio Bio
                Matriz[i][j]=1;
            }else if((i==92 and j==104) or (j==92 and i==104)){//Uniendo Nuble con Estadio Nacional
                Matriz[i][j]=1;
            }else if((i==88 and j==89) or (j==88 and i==89)){//Separando estaciones contiguas qeu no se conectan como Bellas artes y Busta
                Matriz[i][j]=0;
            }else if((i==81 and j==82) or (j==81 and i==82)){// Separando Pudahuel y Lo Prado
                Matriz[i][j]=0;
            }else if((i==35 and j==36) or (j==35 and i==36)){//Separando Santa Ana y Toesca
                Matriz[i][j]=0;
            }else if((i==86 and j==87) or (j==86 and i==87)){//Separando Cumming y Plaza de Armas
                Matriz[i][j]=0;
            }else if((i==102 and j==103) or (j==102 and i==103)){//Separando Presidente Pedro Aguirre Cerda y Bio Bio
                Matriz[i][j]=0;
            }else if((i==103 and j==104) or (j==103 and i==104)){//Separando Bio Bio y Estadio Nacional
                Matriz[i][j]=0;
            }
        }
    }

}


void Mostrar_Matriz(int** Matriz){
    for(int i=0;i<70;i++){
        for(int j=0;j<70;j++){
            cout << Matriz[i][j];
        }
        cout << endl;
    }
}
